﻿using System.Runtime.InteropServices;
using UnityEngine;
using System.Collections;
using Windows.Kinect;
using RootSystem = System;
using System.IO;
using System;

public class DepthSourceManager : MonoBehaviour
{
    private KinectSensor _Sensor;
    private DepthFrameReader _Reader;
    public int FrameWidth = 0;
    public int FrameHeight = 0;
    public uint LengthInPixels = 0;
    public RenderTexture _Texture;
    public ComputeBuffer computeBuffer;
    public ComputeShader computeShader;
    private int computeKernel;
    public RenderTexture GetDepthFrameTexture()
    {
        return _Texture;
    }

    /// <summary>
    /// The _ data is 0 to 65,535 for each ushort (System.UInt16)
    /// </summary>
    private ushort[] _Data;
    //we need a float[] array to pass to the computeshader
    private float[] _FloatData;

    private static DepthSourceManager instance;
    public static DepthSourceManager Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject go = new GameObject();
                go.name = "_DepthSourceManager(Singleton)";
                instance = go.AddComponent<DepthSourceManager>();
            }
            return instance;
        }
    }

    public ushort[] GetData()
    {
	        return _Data;
    }

    private void Awake() 
    {
        //if it's null or if it's the right instance, don't destroy it.
        if (instance == null || instance == this)
            instance = this;
        else
        {
            DestroyImmediate(this);
        }

        _Sensor = KinectSensor.GetDefault();
        
        if (_Sensor != null) 
        {
            _Reader = _Sensor.DepthFrameSource.OpenReader();
            Windows.Kinect.FrameDescription frameDesc = _Sensor.DepthFrameSource.FrameDescription;
            FrameWidth = frameDesc.Width;
            FrameHeight = frameDesc.Height;
            LengthInPixels = frameDesc.LengthInPixels;
            _Data = new ushort[LengthInPixels];
            _FloatData = new float[LengthInPixels];
            _Texture = new RenderTexture(FrameWidth, FrameHeight, 0, RenderTextureFormat.RFloat);
            _Texture.wrapMode = TextureWrapMode.Clamp;
            _Texture.filterMode = FilterMode.Point;
            _Texture.enableRandomWrite = true;
            _Texture.Create();
            computeShader = Resources.Load("DepthComputeShader") as ComputeShader;
            computeKernel = computeShader.FindKernel("DepthFrameCompute");
            computeShader.SetTexture(computeKernel, "Result", _Texture);
            computeBuffer = new ComputeBuffer((int)LengthInPixels, (int)sizeof(float), ComputeBufferType.Raw);
        }
    }
    
    void Update () 
    {
       if (_Reader != null)
        {
            var frame = _Reader.AcquireLatestFrame();
            if (frame != null)
            {
                frame.CopyFrameDataToArray(_Data);
              
                for (int i = 0; i < LengthInPixels; i++)
                    _FloatData[i] = (float)_Data[i];
                
                computeBuffer.SetData(_FloatData);
                computeShader.SetBuffer(computeKernel, "depthBuffer", computeBuffer);
                computeShader.Dispatch(computeKernel, FrameWidth,FrameHeight, 1);
                frame.Dispose();
                frame = null;
            }
        }
    }
    
    void OnApplicationQuit()
    {
        if (computeBuffer != null)
            computeBuffer.Release();

        if(computeBuffer != null)
            computeBuffer.Dispose();

        if (_Reader != null)
        {
            _Reader.Dispose();
            _Reader = null;
        }
        
        if (_Sensor != null)
        {
            if (_Sensor.IsOpen)
            {
                _Sensor.Close();
            }
            
            _Sensor = null;
        }
    }
}
