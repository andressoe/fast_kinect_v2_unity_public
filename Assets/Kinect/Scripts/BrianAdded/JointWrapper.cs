﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Runtime.InteropServices;
using Kinect = Windows.Kinect;


[System.Serializable]
public class JointWrapper{
	[SerializeField]
	public GameObject joint;
	[SerializeField]
	public Kinect.JointType jointType;
}
